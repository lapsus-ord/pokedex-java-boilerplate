package com.nmeo.services;

import com.nmeo.dtos.ModifyPokemonRequest;
import com.nmeo.models.Pokemon;
import com.nmeo.models.PokemonType;

import java.util.*;
import java.util.stream.Collectors;

public class PokemonManager {
    private Set<Pokemon> pokemons;

    public PokemonManager() {
        this.pokemons = new HashSet<>();
    }

    public void add(Pokemon pokemon) throws Exception {
        if (!this.pokemons.add(pokemon)) {
            throw new Exception("this pokemon already exists");
        }
    }

    public List<Pokemon> search(String pokemonName) {
        return this.pokemons.stream().filter(p -> p.pokemonName.contains(pokemonName)).collect(Collectors.toList());
    }

    public List<Pokemon> search(PokemonType pokemonType) {
        return this.pokemons.stream().filter(p -> p.type.equals(pokemonType)).collect(Collectors.toList());
    }

    public void modify(ModifyPokemonRequest modifyPokemonRequest) throws Exception {
        Optional<Pokemon> matchedPokemon = this.pokemons.stream().filter(p -> p.pokemonName.equals(modifyPokemonRequest.pokemonName)).findFirst();
        Pokemon pokemon = matchedPokemon.orElseThrow(() -> new Exception("pokemon not found"));

        if (modifyPokemonRequest.type != null)
            pokemon.type = modifyPokemonRequest.type;

        if (modifyPokemonRequest.lifePoints != null)
            pokemon.lifePoints = modifyPokemonRequest.lifePoints;

        if (modifyPokemonRequest.powers != null)
            pokemon.powers.addAll(modifyPokemonRequest.powers);
    }
}
