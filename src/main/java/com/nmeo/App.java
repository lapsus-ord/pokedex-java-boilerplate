package com.nmeo;

import com.nmeo.dtos.ModifyPokemonRequest;
import com.nmeo.dtos.PokemonSearchResponse;
import com.nmeo.models.Pokemon;
import com.nmeo.models.PokemonType;
import com.nmeo.services.PokemonManager;
import io.javalin.Javalin;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import java.util.List;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class.getName());

    public static void main(String[] args) {
        Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
        logger.info("Pokedex backend is booting...");

        int port = System.getenv("SERVER_PORT") != null ? Integer.parseInt(System.getenv("SERVER_PORT")) : 8080;

        PokemonManager pokemonManager = new PokemonManager();

        Javalin.create()
                .get("/api/status", ctx -> {
                    logger.debug("Status handler triggered");
                    logger.debug(ctx);
                    ctx.status(200);
                })
                .post("/api/create", ctx -> {
                    try {
                        Pokemon pokemon = ctx.bodyAsClass(Pokemon.class);
                        logger.info("newPokemon request: " + pokemon);
                        pokemonManager.add(pokemon);
                    } catch (Exception e) {
                        ctx.status(400);
                    }
                })
                .get("/api/searchByName", ctx -> {
                    String nameToSearch = ctx.queryParam("name");
                    List<Pokemon> matchedPokemons = pokemonManager.search(nameToSearch);
                    logger.info("searchByName: " + matchedPokemons);
                    ctx.json(new PokemonSearchResponse(matchedPokemons));
                })
                .post("/api/modify", ctx -> {
                    try {
                        ModifyPokemonRequest request = ctx.bodyAsClass(ModifyPokemonRequest.class);
                        logger.info("pokemonModifyRequest: " + request);
                        pokemonManager.modify(request);
                        ctx.status(200);
                    } catch (Exception e) {
                        ctx.status(404);
                    }
                })
                .get("/api/searchByType", ctx -> {
                    String typeToSearch = ctx.queryParam("type");
                    try {
                        PokemonType type = PokemonType.valueOf(typeToSearch);
                        List<Pokemon> matchedPokemons = pokemonManager.search(type);
                        logger.info("searchByType " + type + ": " + matchedPokemons);
                        ctx.json(new PokemonSearchResponse(matchedPokemons));
                    } catch (IllegalArgumentException e) {
                        ctx.status(400);
                    }
                })
                .start(port);
    }
}