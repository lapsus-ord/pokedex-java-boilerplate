package com.nmeo.models;

import java.util.Objects;
import java.util.Set;

public class Pokemon {
    public String pokemonName;
    public PokemonType type;
    public int lifePoints;
    public Set<Power> powers;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pokemon otherPokemon = (Pokemon) o;
        return Objects.equals(pokemonName, otherPokemon.pokemonName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pokemonName);
    }

    @Override
    public String toString() {
        return "Pokemon{" +
                "pokemonName='" + pokemonName + '\'' +
                ", type=" + type +
                ", lifePoints=" + lifePoints +
                ", powers=" + powers +
                '}';
    }
}
