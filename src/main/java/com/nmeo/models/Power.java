package com.nmeo.models;

import java.util.Objects;

public class Power {
    public String powerName;
    public PokemonType damageType;
    public int damage;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Power otherPower = (Power) o;
        return Objects.equals(powerName, otherPower.powerName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(powerName);
    }

    @Override
    public String toString() {
        return "Power{" +
                "powerName='" + powerName + '\'' +
                ", damageType=" + damageType +
                ", damage=" + damage +
                '}';
    }
}
