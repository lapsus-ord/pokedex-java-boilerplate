package com.nmeo.dtos;

import com.nmeo.models.Pokemon;

import java.util.List;

public class PokemonSearchResponse {
    public List<Pokemon> result;

    public PokemonSearchResponse(List<Pokemon> matchedPokemons) {
        this.result = matchedPokemons;
    }
}
