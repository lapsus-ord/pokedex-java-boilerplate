package com.nmeo.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.nmeo.models.PokemonType;
import com.nmeo.models.Power;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ModifyPokemonRequest {
    public String pokemonName;

    public PokemonType type;
    public Integer lifePoints;
    public Set<Power> powers;

    @Override
    public String toString() {
        return "ModifyPokemonRequest{" +
                "pokemonName='" + pokemonName + '\'' +
                ", type=" + type +
                ", lifePoints=" + lifePoints +
                ", powers=" + powers +
                '}';
    }
}
